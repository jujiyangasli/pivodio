

import { lazy, Suspense } from 'react';

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

const VideoRecorder = lazy(() => 
  sleep(200).then(() => import('~/components/VideoRecorder'))
);

const SoundRecorder = lazy(() => 
  sleep(200).then(() => import('~/components/SoundRecorder'))
);

export default function IndexRoute() {
  return <div>
    <Suspense fallback={<p>Loading Video Recorder...</p>}>
      <VideoRecorder />
    </Suspense>
    <br /><br />
    <Suspense fallback={<p>Loading Audio Recorder...</p>}>
      <SoundRecorder />
    </Suspense>
    <br />
  </div>;
}
