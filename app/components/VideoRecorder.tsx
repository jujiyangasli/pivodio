import { useReactMediaRecorder } from "react-media-recorder";


export default function VideoRecorder(){
  
  const { status, startRecording, stopRecording, mediaBlobUrl } =
    useReactMediaRecorder({ 
      video: true,
      askPermissionOnMount: true 
    });

    // console.log('video mediaBlobUrl', mediaBlobUrl)

  return (
    <div>
      <p>VIDEO: {status}</p>
      <button onClick={startRecording}>Start Recording</button>&nbsp;
      <button onClick={stopRecording}>Stop Recording</button>
      <br /><br />
      <video src={mediaBlobUrl} controls autoPlay />
    </div>
  );
};