import { useReactMediaRecorder } from "react-media-recorder";
import { useMemo, useState } from 'react'

export default function SoundRecorder(){
  
  const [ audio, setAudio ] = useState<string|null>(null)
  const { status, startRecording, stopRecording } =
    useReactMediaRecorder({ 
      audio: true,
      askPermissionOnMount: true ,
      onStart: () => setAudio(null),
      onStop: (blobUrl) => setAudio(blobUrl)
    });

  // console.log('sound mediaBlobUrl', mediaBlobUrl)
  const audioPlayer = useMemo(() => {
    return audio ? <audio controls autoPlay>
      <source src={audio} type="audio/x-wav" />
      Your browser does not support the audio element.
    </audio> : <p>Audio is null</p>
  },[audio])

  return (
    <div>
      <p>AUDIO: {status}</p>
      <button onClick={startRecording}>Start Recording</button>&nbsp;
      <button onClick={stopRecording}>Stop Recording</button>
      <br /><br />
      {audioPlayer}
    </div>
  );
};